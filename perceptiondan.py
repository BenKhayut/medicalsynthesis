import sys
import mysql.connector
import re
from mysql.connector import Error
from mysql.connector import errorcode
from collections import deque

con = ''
#falskfhsalkfjslkfj;slkfjsla;kjfals;kfj;s
class DL:

	cur = None
	con = None

	def __init__(self, host, database, user, password):
		self.host = host
		self.database = database
		self.user = user
		self.password = password
		global con
		try:
			con = mysql.connector.connect(host=self.host, database=self.database, user=self.user, password=self.password)
			con.autocommit = True
		except mysql.connector.Error as Error:  #, e:
			print("Failed to create connection: {}".format(Error))
			#print "Error %d: %s" % (e.args[0], e.args[1])
			sys.exit(1)

	def __del__(self):
			if self.cur != None:
				self.cur.close()
			if self.con != None:
				con.close()

	def executeSP(self,spName, args, action):  #(action can be fecth/retVal...
		try:
			self.cur = con.cursor()
			if args == None:
				result_args = self.cur.callproc(spName)
			else:
				result_args = self.cur.callproc(spName,args)

			if action == 'fetch':
				global result
				for res in self.cur.stored_results():
					result = res.fetchall()
					print(result)
					return result

			if action == 'retVal':
				return result_args


		except mysql.connector.Error as Error:
			print("Failed to execute stored procedure {}: {}".format(spName,Error))
			#print "Error %d: %s" % (e.args[0], e.args[1])
			sys.exit(1)
			

	#print("connection is closed")
	#
	def find_all_gram_ordered_review_from_thesauri(self):
		return self.executeSP('GetAllItemInGramOrderSentence',None,'fetch')

	##return IWordCont by given IWordID value in IWord table
	def check_value_exist_in_column_table(self, in_id, out_cont):
		print("receieved by method :",in_id, out_cont)
		args = [in_id, out_cont]
		return self.executeSP('CheckValueInColumnOfTabl2',args,'retVal')

	#print("Connection is closed")
	#
	def get_auto_incr_pk_value_by_given_simple_field_val_i(self,in_field, out_pk):
		print ("    2.4.1i) Call function 'get_auto_incr_pk_value_by_given_simple_field_val(self,in_field, out_pk)'':  ", in_field, out_pk) #receive IWordCont, return IWordID
		args = [in_field, out_pk]
		return self.executeSP('ngetPKValueByGivenSimpleFieldValueI',args,'retVal')


	def get_auto_incr_pk_value_by_given_simple_field_val_o(self,in_field, out_pk):
		#print ("    2.4.1o) Call function 'get_auto_incr_pk_value_by_given_simple_field_val_o(self,in_field, out_pk)':  ", in_field, out_pk) #receive OWordCont, return OWordID
		args = [in_field, out_pk]
		return self.executeSP('ngetPKValueByGivenSimpleFieldValueO',args,'retVal')

	#
	def show_version(self):
		self.cur.execute("SELECT VERSION()")
		#print "Database version : %s " % self.cur.fetchone()

	#
	def get_no_auto_incr_pk_value_by_given_simple_field_val(self,in_field, out_no_auto_incr_pk):
		print("   2.3.1)  Called Function  'get_no_auto_incr_pk_value_by_given_simple_field_val(self,in_field, out_no_auto_incr_pk)': ", in_field, out_no_auto_incr_pk)
		args = [in_field, out_no_auto_incr_pk]
		return self.executeSP('ngetPKValueByGivenSimpleFieldValue',args,'retVal')

	def get_IWordCodeC_Max_Value(self,in_iwordcodec, out_max_value):   #IN: 'IWordCodeC' field name in 'IWord' table;
		args = [in_iwordcodec, out_max_value]
		return self.executeSP('ngetIWordCodeCMaxValue',args,'retVal')

	def read_split_on_sentences_review_text_from_txt_file(self, filename):   #"." Splitting list of sentences
		end = 'None'      #end of sentence
		try:                                                       # on tuples (sentences)/
			res = []                                               # each tuple on words
			print("filename " ,filename)
			#with open("review_old.txt") as file:   # review.txt
			with open(filename) as file:   # review.txt
				for line in file:
					for l in re.split(r"(\.)",line):     #\. |\? |! )",line): - #aditional splitters
						res.append(tuple(l.split()))   # rstrip() removes line breaks
#						res.append('.')
#						res.append(end)
			res_cleared = [t for t in res if t != ()]  #remove empty tuples from list of tuples
			return res_cleared  #returned list
			print(res_cleared)

		except IOError:
			msg = ("Unable to read file on disk.")
			file.close()
			return

		finally:
			file.close()

	def get_IWordCodeC_Max_Value(self,in_iwordcodec, out_max_value):   #IN: 'IWordCodeC' field name in 'IWord' table;
		args = [in_iwordcodec, out_max_value]
		return self.executeSP('ngetIWordCodeCMaxValue',args,'retVal')
		#print("Connection is closed")

	def get_OWordCodeC_Max_Value(self,in_owordcodec, out_max_value):   #IN: 'OWordCodeC' field name in 'IWord' table;
		args = [in_owordcodec, out_max_value]
		return self.executeSP('ngetOWordCodeCMaxValue',args,'retVal')

	def get_IWordCodeC_By_Given_IwID_and_IwCont(self, in_IwdID, in_IwrdCont, out_IwrdCodeC):   #find 'IWordCodeC' by 'IWordID and 'IWordCont' from 'IWord'
		args = [in_IwdID, in_IwrdCont, out_IwrdCodeC]
		return self.executeSP('nGetIWordCodeCByGivenIwIDandIwCont',args,'retVal')

	def get_OWordCodeC_By_Given_OwID_and_OwCont(self, on_OwdID, on_OwrdCont, out_OwrdCodeC):   #find 'IWordCodeC' by 'IWordID and 'IWordCont' from 'IWord'
		args = [on_OwdID, on_OwrdCont, out_OwrdCodeC]
		return self.executeSP('nGetOWordCodeCByGivenOwIDandOwCont',args,'retVal')

	def Update_Existed_Record_Vals_In_IWordD(self,in_IwrdCont, in_IWrdCodeL, in_IWrdCodeR, in_IWrdCodeC):   #IN: 'OWordCodeC' field name in 'IWord' table;
		args = [in_IwrdCont, in_IWrdCodeL, in_IWrdCodeR, in_IWrdCodeC]
		self.executeSP('nUpdateExistValsInIWordD',args,'noRet')

	def Update_Existed_Record_Vals_In_OWordD(self, on_OwrdCont, on_OWrdCodeL, on_OWrdCodeR, on_OWrdCodeC):   #IN: 'OWordCodeC' field name in 'OWord' table;
		args = [on_OwrdCont, on_OWrdCodeL, on_OWrdCodeR, on_OWrdCodeC]
		self.executeSP('nUpdateExistValsInOWordD',args,'noRet')

	def Insert_Not_Exist_Vals_In_IWord(self, in_IwrdCont, in_IwrdCodeL, in_IwrdCodeR, in_IwrdCodeC):
		args = [in_IwrdCont, in_IwrdCodeL, in_IwrdCodeR, in_IwrdCodeC]
		self.executeSP('nInsertNotExistValsInIWord',args,'noRet')

	def Insert_Not_Exist_Vals_In_OWord(self, on_OwrdCont, on_OWrdCodeL, on_OWrdCodeR, on_OWrdCodeC):
		print("   2.3.6-ii)  Called Function  'Update_Existed_Record_Vals_In_IWordD': ", on_OwrdCont, on_OWrdCodeL, on_OWrdCodeR, on_OWrdCodeC)
		args = [on_OwrdCont, on_OWrdCodeL, on_OWrdCodeR, on_OWrdCodeC]
		self.executeSP('nInsertNotExistValsInOWord',args,'noRet')

	def Insert_Not_Exist_Vals_In_IOWord(self, in_IWrdID, in_IWrdCodeC, in_OWrdID, in_OWrdCodeC, in_OLngID4, in_OLngCode4, in_ODmID4, in_ODmCode4, in_OSubDmID4, in_OSubDmCode4, in_ILngID4, in_ILngCode4, in_IDmID4, in_IDmCode4, in_ISubDmID4, in_ISubDmCode4):
		args = [in_IWrdID, in_IWrdCodeC, in_OWrdID, in_OWrdCodeC, in_OLngID4, in_OLngCode4, in_ODmID4, in_ODmCode4, in_OSubDmID4, in_OSubDmCode4, in_ILngID4, in_ILngCode4, in_IDmID4, in_IDmCode4, in_ISubDmID4, in_ISubDmCode4]
		self.executeSP('nInsertNotExistValsInIOWord',args,'noRet')

	def percept(self, left, middle, right, status):
		global on_OwCodeC_righ

		Cont_left = left
		Cont_middle = middle
		Cont_right = right

        #these values should be received  as parametters as result of SP with JOIN before call this function 'percept'
		in_OLngID4 = 1 
		in_OLngCode4 = 1
		in_ODmID4 = 1
		in_ODmCode4 = 1
		in_OSubDmID4 = 1
		in_OSubDmCode4 = 1
		in_ILngID4 = 1
		in_ILngCode4 = 1
		in_IDmID4 = 1
		in_IDmCode4 = 1
		in_ISubDmID4 = 1
		in_ISubDmCode4 = 1
		
		if status == 'None':                                    #performed one time (1-st 'left'-''middle'-'left')
		    out_max_value_iwordcodec = 0
		    in_field_name_in_iword_tbl = 'IWordCodeC'
		    maxv_iwordcodec = db.get_IWordCodeC_Max_Value(in_field_name_in_iword_tbl, out_max_value_iwordcodec)        #IWord
		    in_IwCont_left = Cont_left                          #IwContC_left                  +
		    in_IwCodeL_left = 0                                 #IwCodeL_left                  +
		    in_IwCodeC_left = maxv_iwordcodec[1] + 1            #IwCodeC_left                  +

		    out_max_value_owordcodec = 0
		    on_field_name_in_oword_tbl = 'OWordCodeC'
		    maxv_owordcodec = db.get_OWordCodeC_Max_Value(on_field_name_in_oword_tbl, out_max_value_owordcodec)        #OWord

		    on_OwCont_middle = Cont_middle                      #OwCont_middle----->left       ++
		    on_OwCodeL_middle = in_IwCodeC_left                 #OwCodeL_middle---->left       ++
		    on_OwCodeC_middle = maxv_owordcodec[1] + 2          #_OwCodeC_middle               ++  (found max OwCodeC not existed before)
		    in_IwCodeR_left = on_OwCodeC_middle                 #IwCodeR_left                  +
		    on_OwCodeR_middle = maxv_owordcodec[1] + 3          #OwCodeR_middle----->left      ++

		    db.Insert_Not_Exist_Vals_In_IWord(in_IwCont_left, in_IwCodeL_left, in_IwCodeR_left, in_IwCodeC_left)         #Left     into IWord  (i-the)
		    in_IwCodeC_left_ =  in_IwCodeC_left   #!!!!!!!!!!!!

            #insert left & middle codes into IOWord                                                       (into IWord  (i-the))  & into OWord  (o-the)
		    out_pk = 0
		    chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_i(Cont_left, out_pk)
		    in_IWrdID_left = chk_rez[1]                              #in_IWrdID for inseret in IOWord table       !!!!!!!the!!!IWord!
		    in_IWrdID_left_ = in_IWrdID_left      #!!!!!!!!!!!

		    db.Insert_Not_Exist_Vals_In_OWord(on_OwCont_middle, on_OwCodeL_middle, on_OwCodeR_middle, on_OwCodeC_middle)  #Middle   into OWord  (o-fox)
		    db.Insert_Not_Exist_Vals_In_IWord(on_OwCont_middle, on_OwCodeL_middle, on_OwCodeR_middle, on_OwCodeC_middle)  #TEMPLATE into IWord  (i-fox)

		    out_pk = 0
		    chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_o(Cont_middle, out_pk)   #for IWord
		    on_OWrdID_middle = chk_rez[1]   #!!!!!!!!!!!!!!!!!      #on_OWrdID for inseret in IOWord table       !!!!!!!!!fox!!!OWord!!

		    out_max_value_owordcodec = 0
		    on_field_name_in_oword_tbl = 'OWordCodeC'
		    maxv_owordcodec = db.get_OWordCodeC_Max_Value(on_field_name_in_oword_tbl, out_max_value_owordcodec)               #OWord
		    on_OWrdCodeC_middle = maxv_owordcodec[1]         #in_OWrdCodeC  for insert middle word into IOWord table !!!!fox!!!OWord!!!!

		    db.Insert_Not_Exist_Vals_In_IOWord(in_IWrdID_left_, in_IwCodeC_left_, on_OWrdID_middle, on_OWrdCodeC_middle, in_OLngID4, in_OLngCode4, in_ODmID4, in_ODmCode4, in_OSubDmID4, in_OSubDmCode4, in_ILngID4, in_ILngCode4, in_IDmID4, in_IDmCode4, in_ISubDmID4, in_ISubDmCode4)
		else:
		    if status == '':                                    #performed each time (next 'left' (previous -middle)-'middle'(previous-right) - 'right-New')
		        out_pk = 0   
		        chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_i(Cont_left, out_pk)
		        in_IWrdID_left_ins = chk_rez[1]    #!!!!!!!!!!!!!!  #computer the IWrdID - for write into 'IOWord' the current 'left' ('fox')
		        
		        out_IwCodeC = 0
		        chk_rez = db.get_IWordCodeC_By_Given_IwID_and_IwCont(in_IWrdID_left_ins, Cont_left, out_IwCodeC)
		        in_IwCodeC_left_ins = chk_rez[2]   #!!!!!!!!!!!!!!  #computer the OWrdID - for write into 'IOWord' the current 'left' ('fox')
		        
		        out_max_value_owordcodec = 0                        #computer next OwrdCont, OWordCodeL, OWordCodeR, OWordCOdeC for 'middle' ('is')
		        on_field_name_in_oword_tbl = 'OWordCodeC'
		        maxv_owordcodec = db.get_OWordCodeC_Max_Value(on_field_name_in_oword_tbl, out_max_value_owordcodec)        #OWord  !!!

		        on_OwCont_middle = Cont_middle                      #OwCont_left-----         ++
		        on_OwCodeL_middle = maxv_owordcodec[1]              #OwCodeL_left----         ++
		        on_OwCodeC_middle = maxv_owordcodec[1] + 1          #OwCodeC_left             ++  (found max OwCodeC not existed before)
		        if right == '0':
		            on_OwCodeR_middle = int(right)
		        else:
		            on_OwCodeR_middle = maxv_owordcodec[1] + 2          #OwCodeR_left----         ++

		        db.Insert_Not_Exist_Vals_In_OWord(on_OwCont_middle, on_OwCodeL_middle, on_OwCodeR_middle, on_OwCodeC_middle)  #Middle   into OWord  (o-fox)
		        db.Insert_Not_Exist_Vals_In_IWord(on_OwCont_middle, on_OwCodeL_middle, on_OwCodeR_middle, on_OwCodeC_middle)  #TEMPLATE into IWord  (i-'is')

		        out_pk = 0                                #!!!!!!!!!!!!!!  #computer the OWrdID - for write into 'IOWord' the current 'middle!!! current' (is')
		        chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_o(Cont_middle, out_pk)   #for IWord
		        on_OWrdID_middle = chk_rez[1]     #!!!!!!!!!!!!!!!!!     #on_OWrdID for inseret in IOWord table       !!!!!!!!!!!the!!!OWord!

		        out_max_value_owordcodec = 0
		        on_field_name_in_oword_tbl = 'OWordCodeC' #!!!!!!!!!!!!!!  #computer the OWrdCodeC - for write into 'IOWord' the current 'middle!!! current' (is')
		        maxv_owordcodec = db.get_OWordCodeC_Max_Value(on_field_name_in_oword_tbl, out_max_value_owordcodec)               #OWord
		        on_OWrdCodeC_middle = maxv_owordcodec[1]     #!!!!!!!!!!!!!!!!!     #in_OWrdCodeC  for insert middle word into IOWord table !!!!!!the!!!OWord!

		        db.Insert_Not_Exist_Vals_In_IOWord(in_IWrdID_left_ins, in_IwCodeC_left_ins, on_OWrdID_middle, on_OWrdCodeC_middle, in_OLngID4, in_OLngCode4, in_ODmID4, in_ODmCode4, in_OSubDmID4, in_OSubDmCode4, in_ILngID4, in_ILngCode4, in_IDmID4, in_IDmCode4, in_ISubDmID4, in_ISubDmCode4)

	def identify_words(self, list_tuples):
		print("list_of_tuples:  ", list_tuples)  #active

		begin = 'None'    #begin of sentence
		end = 'None'      #end of sentence
		#a = []
		for item in list_tuples:
			if '-' in item[0]:
				item = tuple(item[0].split(' - '))
			len_lst = len(list_tuples)
			#print("List of Tuples; ", len_ls)
		a = deque()  #current "each 1-st" processed words NOT EXIST in the 'IWord' table:
		#m = 0   #index in b[n + m]         #------------(ACT)
		#loop on tuples in list of tuples
		# Analyze 3 word
		#percept(a[0] , a[1], a[2], a[3])
		#popup first element and carry if possible
		#a.pop()
		#continue
		
		out_pk = 0
		status = 'None'  #First - None, #Current - ''
		u = 0
		for w in list_tuples:
			a.clear()
			#a.append(begin)
			for x in w:
			    if u == 0:
			        chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_i(x, out_pk)
			        if chk_rez[1] == None:
			            status = 'None'  #First
			            u = 1
			            a.append(x)
			    else:
			        chk_rez = db.get_auto_incr_pk_value_by_given_simple_field_val_o(x, out_pk)
			        if x != '' or x == '.' and chk_rez[1] == None:
			            if x == '.':
			                a.append(end)
			            else: #?
			                a.append(x)
			            if(len(a) == 3): #or x == '.':   #will executed LEFT popuped and RIGHT Added [the fox is]
			                left = a[0]
			                middle = a[1]
			                right = a[2]
			                if right == end:
			                    right = '0'
			                db.percept(left, middle, right, status)
			                a.popleft()
			                status = ''      #Current
			                continue
			#continue
			#quit()

			#if len(a) > 0 and len(a) < 3:
				#a.append(end)
			#Do processing
			# post processing -- remove first item of a
				#print("processing final",a)
				#a.popleft()
			#return (w)

if __name__ == '__main__':
	try:
	    db = DL('listenapp-db-instance-cluster.cluster-cdpkscglgvea.eu-west-1.rds.amazonaws.com', 'IMNAAI', 'ai', 'imnaai123456')
	    #result = db.find_all_gram_ordered_review_from_thesauri()  #active
	    f = 'dan.txt'
	    list_of_tuples = db.read_split_on_sentences_review_text_from_txt_file(f)  #active
	    print(list_of_tuples)  #active
	    #
	    db.identify_words(list_of_tuples)
	    #print (list_of_tuples)
	except:
	    print("Error  The program ended.\n") 
	    sys.exit(1)
	    #stop()
	#
	#in_id = '1000'
	#out_cont = ''
	#check_rez = db.check_value_exist_in_column_table(in_id, out_cont)
	#print(check_rez)
